python-cassandra-driver (3.20.2-1) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Adam Cecile ]
  * New upstream release 3.20.2.
  * Add several build-dependencies to run unit tests.
  * Enable unit tests.
  * Re-enable Cython which is working fine now.
  * Enable multi-core compilation of Cython files.
  * Switch to GitHub tarball instead of PyPi to have docs and unit tests.
  * Generate Sphinx doc in python3-cassandra-doc package.

 -- Adam Cecile <acecile@le-vert.net>  Tue, 07 Jan 2020 12:43:32 +0100

python-cassandra-driver (3.16.0-2) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * d/changelog: Remove trailing whitespaces.
  * Drop Python 2 support.
  * Use pybuild for building package.
  * Add python3-six to build depends.
  * Enable autopkgtest-pkg-python testsuite.
  * Enable all hardening.
  * d/copyright: Fix cassandra/cmurmur3.c filename.
  * Bump standards version to 4.4.0 (no changes).
  * Bump debhelper compat level to 12.

 -- Ondřej Nový <onovy@debian.org>  Fri, 09 Aug 2019 12:49:01 +0200

python-cassandra-driver (3.16.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Convert git repository from git-dpm to gbp layout
  * d/changelog: Remove trailing whitespaces.

  [ Emmanuel Arias ]
  * New upstream release.
  * Add DPMT to Maintainer field on d/control.
  * Adopt package, add myself to Uploaders (Closes: #888400).
  * Update Standards Version from 4.1.4 to 4.2.1 on
    d/control file.
  * Update on d/copyright file the Copyright file for
    debian/* files.
    - Add me to there.
  * Bump debhelper compatibility to 11 (from 10).
  * Fix problems on d/copyright:
    - Fix dep5-copyright-license-name-not-unique tag lintian
  * Move from old -dbg packages to dbgsym (Closes: #857298).
    - Delete python-cassandra-dbg and python-cassandra[3]-dbg
      from d/control
    - Delete from override_dh_auto_install (on d/rules) the
      installation of *-dbg
    - Change existing -dbg to -dbgsym on d/rules

 -- Emmanuel Arias <emmanuelarias30@gmail.com>  Wed, 05 Dec 2018 21:16:10 +0200

python-cassandra-driver (3.14.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/control: Deprecating priority extra as per policy 4.0.1
  * d/watch: Use https protocol

  [ Sandro Tosi ]
  * New upstream release
  * debian/rules
    - build for all supported python3 versions; patch by Scott Kitterman;
      Closes: #867010
    - remove cassandra/io/asyncioreactor.py from python2 package, code is meant
      to be py3k only
  * debian/copyright
    - extend packaging copyright years
    - update upstream copyright years
  * debian/control
    - bump Standards-Version to 4.1.4 (no changes needed)

 -- Sandro Tosi <morph@debian.org>  Tue, 12 Jun 2018 19:26:05 -0400

python-cassandra-driver (3.7.1-2) unstable; urgency=medium

  * debian/rules
    - remove build and egg-info dirs in clean target, to build the package twice
      in a row; Closes: #825924
  * debian/copyright
    - extend packaging copyright years

 -- Sandro Tosi <morph@debian.org>  Sun, 08 Jan 2017 11:28:48 -0500

python-cassandra-driver (3.7.1-1) unstable; urgency=medium

  * New upstream release
  * debian/rules
    - dont use cython, cassandra-driver is incompatible with 0.25 we have
  * compat level 10

 -- Sandro Tosi <morph@debian.org>  Sat, 17 Dec 2016 13:09:50 -0500

python-cassandra-driver (3.7.0-1) unstable; urgency=medium

  * New upstream release
  * debian/control
    - add dh-python t0 b-d

 -- Sandro Tosi <morph@debian.org>  Sun, 02 Oct 2016 14:23:02 -0400

python-cassandra-driver (3.4.1-1) unstable; urgency=medium

  [ Sandro Tosi ]
  * New upstream release
  * debian/control
    - adjust Vcs-Browser to DPMT standards
    - add libev-dev to b-p needed by cassandra.io.libevwrapper
    - bump Standards-Version to 3.9.8 (no changes needed)
  * debian/copyright
    - extend packaging copyright years
    - update upstream copyright years
  * build arch:any and debug pkgs now that this prj contains extensions

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

 -- Sandro Tosi <morph@debian.org>  Sun, 12 Jun 2016 22:58:39 +0100

python-cassandra-driver (2.5.1-1) unstable; urgency=low

  * Initial release (Closes: #771065)

 -- Sandro Tosi <morph@debian.org>  Thu, 25 Jun 2015 15:50:21 -0400
